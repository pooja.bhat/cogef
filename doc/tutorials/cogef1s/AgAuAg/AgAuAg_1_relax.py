# creates: 1scogef.png
from ase import io
from ase.atoms import Atoms
from ase.optimize import FIRE
from ase.calculators.emt import EMT

fmax = 0.01

fname = 'AgAuAg.traj'
try:
    image = io.read(fname)
except FileNotFoundError:
    image = Atoms('AgAuAg', positions=((-1, 0, 0), (0, 0, 0), (1, 0, 0)))
    image.calc = EMT()
    FIRE(image).run(fmax=fmax)
    image.write(fname)
