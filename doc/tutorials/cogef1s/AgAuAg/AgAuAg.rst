============
AgAuAg chain
============

We consider the linear chain of three atoms AgAuAg here, which we
relax in the first step:

.. literalinclude:: AgAuAg_1_relax.py

We will stretch this configuration on the two outer atoms.

Symmetric cogef
===============

The standard COGEF pulling script is applied:

.. literalinclude:: AgAuAg_2_1scogef.py

which creates the directory ``cogef1d_0_2`` and
writes the file ``cogef1d_0_2/cogef.traj``
containing the corresponding trajectory.

.. image:: symmetric_vs_rattle.png

This produces rather high energies ("symmetric" above) as both
AgAu bondlengths are stretched equally.

Symmetry breaking through rattle
================================

We may break symmetry by using ``rattle``:

.. literalinclude:: AgAuAg_3_rattle.py

which creates the directory ``rattle_0_2`` and
writes the file ``rattle_0_2/cogef.traj``.
The symmetry breaking enables one of the bonds to be intact,
while the other breaks. The figure shows that this assymmetric
configuration is of lower energy.
		    
